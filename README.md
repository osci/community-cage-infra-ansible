# Community Cage Infrastructure Management using Ansible

## Introduction

We offer a range of full-lifecycle services for open source community projects that are strategic to Red Hat. Services range from managed hosting to server colocation (ping/power/pipe/racks) in multiple data centers. We also provide project planning & execution, budget planning, and purchasing support for tenants.

This is a physical co-location, managed + virtual services including private cloud, and public cloud offering from the Open Source Program Office (OSPO) working with Regional IT, ACS, and PnT DevOps.

This repository contains Ansible rules to manage shared services and VM offering for all tenants of the cage.

You need Ansible-core >=2.13.

## Admin-specific Production Settings

You can use `group_vars/all/local_settings.yml` for you local
settings like `ansible_become_pass` if your computer storage is
encrypted. Use `--ask-sudo-pass` if you don't want to use this
method. Currently Ansible is unable to ask _when needed_ so
the global setting has been disabled in `ansible.cfg`.

## Dealing with Secrets

We use Ansible Vault (`ansible-vault` command) to hide some parameters
like service credentials or emails to avoid SPAM.

To make it easy all such files are named '\*.vault.yml' and git
attributes are defined to make diff-ing and merging easy.

Your config needs to be enhanced to tell git how to handle these files.
This is very easy, look at this URL for more info:
https://github.com/building5/ansible-vault-tools

## Improving Ansible Speed

Ansible is slow, but there's a nice project to improve its performance. It still has glitches so it's not enabled by default, but it's easy to enable it.

First install the library (it is not yet packaged):

pip install mitogen

Then you just need to run playbooks this way:

ANSIBLE_STRATEGY=mitogen_linear ansible-playbook …

## Tenants information

Tenants information are used to share contacts among our team. It looks like this:

---

tenant:
name: "Human name for the project"
community_cage: true
contacts:
"Jean Dupont": - 'mailto:jean.dupont@example.com' - 'irc:jdupont@freenode'
urls:
'website': https://project-handle.example.com/
notes: - "Jean recently stepped down from project maintainership, getting in touch the new owner"

These data are defined for each <tenant> in two files:

- `group_vars/tenant_<tenant>/tenant.yml`: name, public links
- `group_vars/tenant_<tenant>/tenant.vault.yml`: contacts and sensitive links or notes

Possible fields:

- `community_cage`: advertize this tenant is a Community Cage tenant (aside from other hosting) (defaults to False).
- `contacts`: people we've been in touch with when working with the project. For each one we keep a list of contact methods defined as [URIs](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier) (mailto…). There is no scheme to express IRC identities, thus we'll follow [PSYC's proposal](https://about.psyc.eu/IRC_URI).
- `urls`: a list of related documents: website, infra team's documentation…
- `notes`: an optional list of comments.

For tenants only using the shared services (with no associated hosts), these data are defined in these files in the `hostless_tenants` hash:

- `group_vars/tenant_osci/tenant.vault.yml`
- `group_vars/tenant_osci/tenant.yml`
  (the key is the tenant name <tenant> and the data is the same as above)
