---

# This playbook does:
# - preliminary checks
# - copy the VM provisioning data from the source to the destination
#   (files created by the `guest_virt_install` role)
# - create LVs used by the VM on destination with the same size as the source
# - stops the VM on the source and disables autostart
# - copy the LVs data from the source to the destination
#   (encrypted and compressed via the OSCI-Services VLAN and temporary firewalld rules)
# - starts the VM on the destination and enables autostart
#
# Additionally this playbooks checks for and sets the `moved` flag to avoid mistakes.
#
# This playbooks does not do any cleanup on the source. Once everything is fine,
# please use the `cleanup_vm.yml` playbook.
#

# You can skip the prompt by passing vars using the `-e` option

- name: "Move a VM to a new hypervisor"
  hosts: localhost
  vars:
    vm_copy_port: 10421

  vars_prompt:
    - name: hyp_src
      prompt: "Please provide the source hypervisor"
      private: no

    - name: hyp_dst
      prompt: "Please provide the destination hypervisor"
      private: no

    - name: vm_name
      prompt: "Please provide the VM name (domain)"
      private: no

  tasks:
    - name: "Fetch hypervisors facts"
      ansible.builtin.setup:
      loop:
        - "{{ hyp_src }}"
        - "{{ hyp_dst }}"
      delegate_to: "{{ item }}"
      delegate_facts: True

    - name: "Fetch list of VMs in source hypervisor"
      community.libvirt.virt:
        command: list_vms
      register: src_vms_list
      delegate_to: "{{ hyp_src }}"

    - name: "Check that the VM already exists on the source"
      ansible.builtin.fail:
        msg: "VM does not exist on the source hypervisor"
      when: vm_name not in src_vms_list.list_vms

    - name: "Fetch list of VMs in destination hypervisor"
      community.libvirt.virt:
        command: list_vms
      register: dst_vms_list
      delegate_to: "{{ hyp_dst }}"

    - name: "Check that the VM does not already exist on the destination"
      ansible.builtin.fail:
        msg: "VM already exists on the destination hypervisor"
      when: vm_name in dst_vms_list.list_vms

    - name: "Check if VM was moved"
      ansible.builtin.stat:
        path: "{{ hostvars[hyp_src].virt_moved_flag }}"
      register: vm_moved_flag_res
      delegate_to: "{{ hyp_src }}"

    - name: "Warn when VM was moved"
      ansible.builtin.pause:
        prompt: "VM previously moved from this hypervisor; if you are sure press ENTER, else press Ctrl-C"
      when: vm_moved_flag_res.stat.exists

    - name: "Check if hypervisors have the shared VG configured"
      ansible.builtin.stat:
        path: "/dev/{{ hostvars[hyp_src].virt_shared_vg }}"
      register: vm_shared_vg_res
      loop:
        - "{{ hyp_src }}"
        - "{{ hyp_dst }}"
      delegate_to: "{{ item }}"

    - name: "Warn if the shared VG is missing"
      ansible.builtin.fail:
        msg: "The shared VG is missing on {{ item.item }} hypervisor"
      loop: "{{ vm_shared_vg_res.results }}"
      loop_control:
        label: "{{ item.item }}"
      when: not item.stat.exists

    - name: "Install tools"
      ansible.builtin.package:
        name:
          - netcat
          - pigz
      loop:
        - "{{ hyp_src }}"
        - "{{ hyp_dst }}"
      delegate_to: "{{ item }}"

    - name: "Find the VM provisioning config"
      ansible.builtin.find:
        paths:
          - "/etc/libvirt/kickstarts/{{ vm_name }}"
          - "/etc/libvirt/preseeds/{{ vm_name }}"
        file_type: file
      register: vm_prov_files_res
      delegate_to: "{{ hyp_src }}"

    - name: "Create the VM provisioning config directories"
      # noqa risky-file-permissions
      ansible.builtin.file:
        path: "{{ item.path | dirname }}"
        state: directory
      loop: "{{ vm_prov_files_res.files }}"
      delegate_to: "{{ hyp_dst }}"

    - name: "Create a local temporary directory for file transfer"
      ansible.builtin.tempfile:
        state: directory
      register: vm_prov_files_tmpdir

    - name: "Copy VM provisioning files locally"
      ansible.builtin.fetch:
        src: "{{ item.path }}"
        dest: "{{ vm_prov_files_tmpdir.path }}/"
      loop: "{{ vm_prov_files_res.files }}"
      loop_control:
        label: "{{ item.path }}"
      delegate_to: "{{ hyp_src }}"

    - name: "Copy VM provisioning files on destination"
      ansible.builtin.copy:
        # fetch reproduces the full original path in the destination directory
        # and prefix it with the inventory_hostname (but it's not delegate-aware)
        src: "{{ vm_prov_files_tmpdir.path }}/localhost/{{ item.path }}"
        dest: "{{ item.path }}"
        owner: "{{ item.pw_name }}"
        group: "{{ item.gr_name }}"
        mode: "{{ item.mode }}"
      loop: "{{ vm_prov_files_res.files }}"
      loop_control:
        label: "{{ item.path }}"
      no_log: True
      delegate_to: "{{ hyp_dst }}"

    - name: "Remove the local temporary directory for file transfer"
      ansible.builtin.file:
        path: "{{ vm_prov_files_tmpdir.path }}"
        state: absent

    - name: "Get VM Config"
      community.libvirt.virt:
        command: get_xml
        name: "{{ vm_name }}"
      register: vm_config_res
      delegate_to: "{{ hyp_src }}"

    - name: "Get list of LVs to transfer"
      community.general.xml:
        xmlstring: "{{ vm_config_res.get_xml }}"
        xpath: /domain/devices/disk/source
        content: attribute
      register: vm_config_disks_path
      delegate_to: "{{ hyp_src }}"

    - name: "Create the LV on the destination"
      vars:
        lv: "{{ disk.source.dev | basename }}"
      community.general.lvol:
        lv: "{{ lv }}"
        vg: "{{ hostvars[hyp_dst].volgroup }}"
        size: "{{ hostvars[hyp_src].ansible_lvm.lvs[lv].size_g }}G"
        state: present
      loop: "{{ vm_config_disks_path.matches }}"
      loop_control:
        label: "{{ lv }}"
        loop_var: disk
      delegate_to: "{{ hyp_dst }}"

    - name: "Stop VM on source hypervisor"
      community.libvirt.virt:
        command: shutdown
        name: "{{ vm_name }}"
      register: virt_shutdown
      # the virt module is not idempotent
      failed_when: virt_shutdown.failed and 'domain is not running' not in virt_shutdown.msg
      delegate_to: "{{ hyp_src }}"

    - name: "Wait for the VM to shutdown"
      community.libvirt.virt:
        command: status
        name: "{{ vm_name }}"
      register: virt_status
      until: virt_status.status == 'shutdown'
      retries: 60
      delay: 1
      delegate_to: "{{ hyp_src }}"

    - name: "Disable VM autostart on source hypervisor"
      community.libvirt.virt:
        autostart: False
        name: "{{ vm_name }}"
      delegate_to: "{{ hyp_src }}"

    - name: "Update VM config to modern machine"
      vars:
        vm_config_cpu_update: "{{ vm_config_res.get_xml | regex_replace(\"<cpu (?s).+</cpu>\", \"<cpu mode='host-passthrough'/>\", multiline=True) | regex_replace(\"machine='pc[^']+'\", \"machine='q35'\") | regex_replace(\"pci-root\", \"pcie-root\") | replace('/dev/' + hostvars[hyp_src].volgroup , '/dev/' + hostvars[hyp_src].virt_shared_vg) }}"
      community.libvirt.virt:
        command: define
        xml: "{{ vm_config_cpu_update }}"
      when: vm_config_cpu_update != vm_config_res.get_xml
      delegate_to: "{{ hyp_src }}"

    - name: "Generate password for the transfer"
      ansible.builtin.set_fact:
        xfer_pwd: "{{ lookup('password', '/dev/null chars=ascii_lowercase,ascii_uppercase,digits length=24') }}"

    - name: "Open the firewall port to copy the VM data"
      ansible.posix.firewalld:
        port: "{{ vm_copy_port }}/tcp"
        state: enabled
        immediate: yes
      delegate_to: "{{ hyp_dst }}"

    - name: "Copy LV"
      ansible.builtin.include_tasks: includes/copy_vm_lv.yml
      vars:
        # TODO: investigate deeper why using the original LV path fails on Jerry
        #       is the shell command too long?
        #       switching to the shared path works fine
        lv_path: "{{ disk.source.dev | replace('/dev/' + hostvars[hyp_src].volgroup , '/dev/' + hostvars[hyp_src].virt_shared_vg) }}"
        xfer_vlan_id: "{{ cage_vlans['OSCI-Services'].vlan_id }}"
        xfer_dst_ip: "{{ hostvars[hyp_dst]['ansible_virbr' + xfer_vlan_id].ipv4.address }}"
        xfer_openssl_opt: "{{ ((hostvars[hyp_src].ansible_os_family != 'RedHat' or hostvars[hyp_src].ansible_distribution_major_version|int >= 8) and (hostvars[hyp_dst].ansible_os_family != 'RedHat' or hostvars[hyp_dst].ansible_distribution_major_version|int >= 8)) | ternary('-pbkdf2', '') }}"
      loop: "{{ vm_config_disks_path.matches }}"
      loop_control:
        label: "{{ lv_path }}"
        loop_var: disk

    - name: "Close the firewall port used to copy the VM data"
      ansible.posix.firewalld:
        port: "{{ vm_copy_port }}/tcp"
        state: disabled
        immediate: yes
      delegate_to: "{{ hyp_dst }}"

    - name: "Migrate VM metadata"
      ansible.builtin.command: "virsh -c qemu+ssh://{{ hyp_src }}/system migrate --offline --persistent --abort-on-error --verbose {{ vm_name }} qemu+ssh://{{ hyp_dst }}/system"
      changed_when: True

    - name: "Start and enable VM autostart on destination hypervisor"
      community.libvirt.virt:
        autostart: True
        command: start
        name: "{{ vm_name }}"
      delegate_to: "{{ hyp_dst }}"

    - name: "Mark VM as moved on the source"
      ansible.builtin.copy:
        content: ""
        dest: "{{ hostvars[hyp_src].virt_moved_flag }}"
        owner: root
        group: root
        mode: "0644"
      delegate_to: "{{ hyp_src }}"

    - name: "Unmark VM as moved on the destination"
      ansible.builtin.file:
        path: "{{ hostvars[hyp_dst].virt_moved_flag }}"
        state: absent
      delegate_to: "{{ hyp_dst }}"

    - name: "Advice about cleanup"
      ansible.builtin.debug:
        msg: "The VM on the source is stopped and autostart disabled; when you are sure all is fine you can cleanup all the things using the  `cleanup_vm.yml` playbook"

