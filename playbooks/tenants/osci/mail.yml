---

- name: "Deploy antispam"
  hosts: mail_servers
  roles:
    - role: postgrey
      whitelist_clients: "{{ cage_subnets }}"
    - role: spamassassin
      service_profile: low
  tags: antispam

- name: "Deploy MTA-STS resolver"
  hosts: mail_servers
  tasks:
    - name: "Install MTA-STS resolver for postfix"
      ansible.builtin.include_role:
        name: mta_sts_resolver
      vars:
        cache_type: sqlite
        debug: True
  tags:
    - mta-sts
    - mta_sts_resolver

- name: "Prepare SMTP TLS certificates"
  hosts: mail_servers
  tasks:
    - name: "Generate SMTP certificates"
      ansible.builtin.include_role:
        name: letsencrypt_rfc2136
        tasks_from: gen_cert
      vars:
        mail_domain: osci.io
        domain: "{{ mx.dns_name }}"
  tags: mta_cert

# MTA MX1
- name: "Deploy primary MTA"
  hosts: mail_servers_mx1
  tasks:
    - name: "Tenants hosted mail domains: compute parameters"
      ansible.builtin.include_tasks: mail_tenant_pre.yml
      with_dict: "{{ tenant_hosted_mail_domains }}"
      loop_control:
        loop_var: tenant
    - name: Install MTA
      ansible.builtin.include_role:
        name: postfix
      vars:
        myhostname: "{{ mx.dns_name }}"
        mydestination: "{{ mx_domains_int }}"
        relay_domains: "{{ mx_domains_ext }}"
        mynetworks: "{{ cage_subnets }}"
        auth: sasldb
        with_postgrey: true
        with_spamassassin: true
        with_postsrsd: true
        smtpd_options:
          content_filter: spamfilter
        local_accounts:
          # tenant: osci
          - ticket
          - ticket-comment
          - ticket-rdo-cloud
          - ticket-rdo-cloud-comment
          # tenant: gdb
          - gnutoolchain-gerrit
          # tenant: pulp
          - pulp-discourse
        aliases:
          root: "{{ ['root'] + comminfra_tech_emails }}"
          contact: root
          security: root
          infra: root
          hostmaster: root
          webmaster: root
          openshift-admin: root
          no-reply: /dev/null
          comminfra: "{{ comminfra_team_emails }}"
          jetpack-admin: "{{ jetpack_admins_emails }}"
          instructlab-sec: "{{ instructlab_sec_emails }}"
          instructlab-cocc: "{{ instructlab_cocc_emails }}"
          # archive for edk2 list
          edk2-devel-archive: /var/mail/edk2-devel-archive/mbox
        tls_method: manual
        cert_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/fullchain.pem"
        key_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/privkey.pem"
        with_mta_sts: True
    - name: Create smtp users
      ansible.builtin.include_role:
        name: sasldb
      vars:
        db_group: postfix
        user_create:
          - "{{ discourse_os_smtp_user }}"
          - "{{ listmonk_os_smtp_user }}"
          - "{{ pretalx_os_smtp_user }}"
    - name: "Tenants hosted mail domains: install assets"
      ansible.builtin.include_tasks: mail_tenant_post.yml
      with_dict: "{{ tenant_hosted_mail_domains }}"
      loop_control:
        loop_var: tenant
  tags: mta

# MDA
- name: "Deploy MDA"
  hosts: mail_servers_mx1
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/osci/mail/"
  tasks:
    - name: "Generate SMTP certificates"
      ansible.builtin.include_role:
        name: letsencrypt_rfc2136
        tasks_from: gen_cert
      vars:
        mail_domain: osci.io
        domain: "{{ osci_mda }}"

    - name: "Install Dovecot"
      ansible.builtin.include_role:
        name: dovecot
      vars:
        auth: yaml-dict
        cert_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/cert.pem"
        key_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/privkey.pem"
        ca_file: "{{ pki.le_cert_path }}/{{ osci_mda }}/chain.pem"
        # used by discourse
        with_pop3: True

    - name: install Dovecot users
      ansible.builtin.copy:
        src: "{{ data_dir }}/users/"
        dest: /etc/dovecot/users/
        owner: root
        group: dovecot-proxy
        mode: "0640"

    # LE hooks order:
    #   - 03: deploy cert if needed (copy and change perms for non-root services)
    #   - 07: restart/reload/systemctl kill/… to notify the service to take the new cert into account
    # other levels for special actions
    - name: "Install Let's Encrypt renewal hook"
      ansible.builtin.template:
        src: "{{ data_dir }}/07_dovecot_cert_renewal_restart_service"
        dest: /etc/letsencrypt/renewal-hooks/deploy/
        owner: root
        group: root
        mode: "0755"
  tags: mda

# MTA MX2
- name: "Deploy secondary MTA"
  hosts: mail_servers_mx2
  tasks:
    - name: Install MTA
      ansible.builtin.include_role:
        name: postfix
      vars:
        myhostname: "{{ mx.dns_name }}"
        mydestination: []
        relay_domains: "{{ mx_domains_int | union(mx_domains_ext) | union(tenant_hosted_mail_domains.values() | sum(start = [])) }}"
        mynetworks: "{{ cage_subnets }}"
        with_postgrey: true
        with_spamassassin: true
        smtpd_options:
          content_filter: spamfilter
        tls_method: manual
        cert_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/fullchain.pem"
        key_file: "{{ pki.le_cert_path }}/{{ mx.dns_name }}/privkey.pem"
        with_mta_sts: True
  tags: mta

- name: "Generate MTA-STS policy"
  hosts: www.osci.io
  vars:
    domain: osci.io
    mx1_dns_names: "{{ groups['dns_servers_ns1'] | map('extract', hostvars) | map(attribute='mx') | map(attribute='dns_name') | list }}"
    mx2_dns_names: "{{ groups['dns_servers_ns2'] | map('extract', hostvars) | map(attribute='mx') | map(attribute='dns_name') | list }}"
  tasks:
    - name: "Install MTA-STS for OSCI domain"
      ansible.builtin.include_role:
        name: mta_sts_policy
      vars:
        document_root: "/var/www/{{ website_domain }}"
        mx_list: "{{ mx1_dns_names + mx2_dns_names }}"
        dns_host: polly.osci.io
        dns_file: "/etc/named/masters/{{ domain }}.mta-sts"
  tags:
    - mta_sts
    - mta_sts_policy

- name: "Deploy OSCI Announcement System"
  tags: announce
  hosts: openshift_controller
  module_defaults:
    group/kubernetes.core.k8s:
      api_key: "{{ openshift_dedicated_api_key }}"
      host: "{{ openshift_dedicated_host }}"
  tasks:
    - name: "Deploy ListMonk"
      ansible.builtin.include_role:
        name: openshift-apps/listmonk
      vars:
        project: prod-osci-announce
        vhost: announce.osci.io
        database_storage_size: 1
        storage_size: 1

- name: "Deploy OSCI Mailing-Lists"
  hosts: lists.osci.io
  tags: mailing_lists
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/osci/mail/lists/"

  tasks:
    - name: "Install Mailing-Lists Instance"
      include_role:
        name: mailing_lists_server_containers
      vars:
        mail_domain: lists.osci.io
        mail_aliases:
          root: "{{ ['root'] + comminfra_tech_emails }}"
          listmaster: root
        whitelist_clients:
          - redhat.com
        site_admins:
          - duck
        with_fedora_auth: True

    - name: "Create Mail Templates directories"
      file:
        path: "/home/mailman/core/var/templates/{{ item }}"
        state: directory
        owner: mailman
        group: mailman
        mode: "0755"
      loop:
        - site
        - site/en

    - name: "Install Mail Templates"
      ansible.builtin.copy:
        src: "{{ data_dir }}/mail_templates/{{ item }}"
        dest: "/home/mailman/core/var/templates/{{ item }}"
        owner: mailman
        group: mailman
        mode: "0644"
      loop:
        - "site/en/list:member:regular:footer.txt"
        - "site/en/list:member:digest:footer.txt"

    - name: "Install custom branded navbar template"
      ansible.builtin.copy:
        src: "{{ data_dir }}/navbar-brand.html"
        dest: "/home/mailman/web/templates/hyperkitty/"
        owner: mailman
        group: mailman
        mode: "0644"
      notify: Restart Mailman Web container

    - name: "Install custom logo"
      ansible.builtin.copy:
        src: "{{ data_dir }}/{{ item }}"
        dest: "/home/mailman/web/static-extra/"
        owner: mailman
        group: mailman
        mode: "0644"
      loop:
        - tenant_logo.png
        # TODO: the website favicon is broken; when fixed let's use it here too
        #- favicon.ico
      notify: Restart Mailman Web container

