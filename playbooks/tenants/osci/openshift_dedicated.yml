---

- name: "Configure OpenShift projects"
  hosts: openshift_controller
  module_defaults:
    group/kubernetes.core.k8s:
      api_key: "{{ openshift_dedicated_api_key }}"
      host: "{{ openshift_dedicated_host }}"
  roles:
    - role: openshift/ansible-dependencies

    - role: openshift/subscription
      subscription_name: elasticsearch-operator
      namespace: openshift-operators-redhat

    - role: openshift/subscription
      subscription_name: cluster-logging
      namespace: openshift-logging

    - role: openshift/subscription
      subscription_name: openshift-cert-manager-operator
      namespace: cert-manager-operator

    - role: openshift/projects
      prefix: redhat.com
      # dedicated-admins is managed out of band for now
      # since the data is synced with cloud.redhat.com and
      # openshift hive (https://www.openshift.com/blog/openshift-hive-cluster-as-a-service)
      os_groups:
        osci-admins:
          - mscherer
          - mdequene
        gluster-infra:
          - dkhandel
          - rkamath
          - mscherer
        quarkus-admins:
          - ggastald
          - manderse
          - gsmet
          - mszynkie
          - olubyans
        quarkus-lottery-admins:
          - gsmet
          - yrodiere
          - mbekhta
        quarkus-game-admins:
          - adamevin
          - ggastald
        quarkus-search-admins:
          - gsmet
          - ggastald
          - yrodiere
          - mbekhta
        langchain4j-admins:
          - gsmet
          - ggastald
          - dliubars
        theforeman-admins:
          - evgeni
          - mcorr
          - tbrisker
          - ehelms
          - ekohlvan
        ldapwalker-admins:
          - mdequene
        knative-admins:
          - jberkus
        istio-admins:
          - jberkus
        carbon-ninja-admins:
          - daggarwa
          - kness
          - jtorcass
          - dkhater
          - zleblanc
        eightknot-admins:
          - cdolfi
          - jkunstle
          - gsutclif
          - clatschk
        appservices-tech-admins:
          - gryan
          - tbentley
          - ebernard
          - wtrocki
        pulp-admins:
          - bmbouter
          - mdellweg
          - dkliban
        apicurio-admins:
          - pantinor
          - aperuffo
          - carnalca
          - ewittman
          - jsenko
          - riprasad
        pride-admins:
          - aoyawale
        keycloak-admins:
          - boliveir
          - sthorger
          - vmuzikar
          - psilva
        josdk-admins:
          - claprun
          - aperuffo
          - ameszaro
        wildfly-admins:
          - mstefank
          - pberan
          - mjusko
          - khermano
        devconf-admins:
          - jridky
        jkube-admins:
          - rokumar
          - mnurisan
        ansible-admins:
          - gsutclif
          - wbentley
          - dnaro
          - lgallego
          - ssydoren
          - carchen
        patchback-admins:
          - ssydoren
        mvnpm-admins:
          - pkruger
          - adamevin
        hibernate-admins:
          - yrodiere
          - mbekhta
        llvm-admins:
          - tstellar
        camel-jbang-admins:
          - gfournie
        ossupstream-admins:
          - jcobb

      # production project start by prod-
      # it should (if possible) reflect the domain name of the
      # website for easier retrival
      os_projects:
        # need to be created manually, since the operator installer didn't with our config
        #
        cert-manager-operator:
          description: "Cert-manager-operator"
          display: "cert-manager-operator"

        prod-arcade-rh-com:
          description: "Gaming CoP website, with demo of opensource games"
          display: "arcade.redhat.com"
          users:
            - jsprague
            - ejacobs
            - rkieley
            - mclayton
            - jdudash
            - rcoffman

        prod-fixtures-pulpproject-org:
          description: "Static website used for pulp automated tests"
          display: "fixtures.pulpproject.org"

        prod-notes-theopenorganization-org:
          description: "Hedgedoc instance for The Open Org"
          display: "notes.theopenorganization.org"

        prod-quarkus-bot:
          description: "Quarkus bot"
          display: "Quarkus bot"
          groups:
            - quarkus-admins

        prod-quarkus-zulip-bot:
          description: "Quarkus zulip bot"
          display: "Quarkus zulip bot"
          groups:
            - quarkus-admins

        prod-registry-quarkus-io:
          description: "Quarkus registry"
          display: "registry.quarkus.io"
          groups:
            - quarkus-admins

        prod-rhpride-transition:
          description: "Future website for our Pride ERG"
          display: "url to be added"
          groups:
            - pride-admins

        prod-status-quarkus-io:
          description: "Status of Quarkus CI"
          display: "status.quarkus.io"
          groups:
            - quarkus-admins

        prod-prprocessor-theforeman-org:
          description: "Github bot for Theforeman"
          display: "prprocessor.theforeman.org"
          groups:
            - theforeman-admins

        test-prprocessor-theforeman-org:
          description: "Github bot for Theforeman"
          display: "prprocessor.theforeman.org"
          groups:
            - theforeman-admins

        prod-projects-theforeman-org:
          description: "Redmine instance of Theforeman project"
          display: "projects.theforeman.org"
          groups:
            - theforeman-admins

        prod-ldapwalker-demo-duckcorp-org:
          description: "LDAPWalker Demo"
          display: "ldapwalker-demo.duckcorp.org"
          groups:
            - ldapwalker-admins

        prod-clc-osci-io:
          description: "Conscious language checker instance for Rich B"
          display: "Conscious language checker"

        prod-elections-knative-dev:
          description: "Elekto setup for Knative.dev community"
          display: "elections.knative.dev"
          groups:
            - knative-admins

        prod-elections-istio-io:
          description: "Elekto setup for Istio.io community"
          display: "elections.istio.io"
          groups:
            - istio-admins

        test-carbon-ninja:
          description: "Dev environment for carbon ninja"
          display: "Carbon ninja test env"
          groups:
            - carbon-ninja-admins

        prod-eightknot:
          description: "Prod tenant for Eightknot"
          display: "Prod tenant for Eightknot"
          groups:
            - eightknot-admins

        dev-eightknot:
          description: "Dev tenant for Eightknot"
          display: "Dev tenant for Eightknot"
          groups:
            - eightknot-admins

        prod-analytics-pulpproject-org:
          description: "Analytics for the Pulp Projects"
          display: "Pulp Analytics"
          groups:
            - pulp-admins

        dev-analytics-pulpproject-org:
          description: "Analytics for the Pulp Projects (Development)"
          display: "Pulp Analytics (DEV)"
          groups:
            - pulp-admins

        prod-apicurio-apps:
          description: "Apicurio projects automation infrastructure"
          display: "Apicurio projects automation infrastructure"
          groups:
            - apicurio-admins

        stage-apicurio-apps:
          description: "Apicurio projects automation infrastructure, stage project"
          display: "Apicurio projects automation infrastructure, stage project"
          groups:
            - apicurio-admins

        prod-quarkus-github-lottery:
          description: "Totally not a casino or anything illegal regarding quarkus"
          display: "Quarkus Github Lottery"
          groups:
            - quarkus-lottery-admins

        prod-quarkus-game:
          description: "Game for Quarkus devs"
          display: "Game for Quarkus devs"
          groups:
            - quarkus-game-admins

        prod-keycloak-bot:
          description: "Github bot for Keycloak"
          display: "Github bot for Keycloak"
          groups:
            - keycloak-admins

        prod-josdk-dev:
          description: "Support infrastructure for Java Operator Framework Development"
          display: "Support infrastructure for Java Operator Framework Development"
          groups:
            - josdk-admins

        prod-wildfly-github-bot:
          description: "Github bot for Wildfly"
          display: "Github bot for Wildfly"
          groups:
            - wildfly-admins

        test-gluster-softserve-port:
          description: "Project to test port of softserve to python3"
          display: "Project  to test port of softserve to python3"
          groups:
            - gluster-infra

        # not managed by us, will be removed once unused
        # here so people can play with it freely by changing things in the config, etc
        test-analytics-pulp:
          description: "Test namespace for mdellweg to debug - can likely be be removed in sept 2023"
          display: "Test namespace for dev of analytics.pulpproject"
          groups:
            - pulp-admins

        prod-cfp-devconf-cz:
          description: "Tenant used for various devconf.cz related container"
          display: "Numerous devconf.cz sites"
          groups:
            - devconf-admins

        prod-pretalx-devconf-info:
          description: "Tenant used for pretalx.devconf.info containers"
          display: "pretalx.devconf.info"
          groups:
            - devconf-admins

        prod-forge-jboss-org:
          description: "Forge.jboss.org website"
          display: "Forge.jboss.org website"
          users:
            - ggastald

        prod-jkube-ci:
          description: "Tenant to run CI test for JKube"
          display: "Tenant to run CI test for JKube"
          groups:
            - jkube-admins

        prod-fabric8-kubernetes-client-ci:
          description: "Tenant to run CI test for Fabric8 kube client"
          display: "Tenant to run CI test for Fabric8 kube client"
          groups:
            - jkube-admins

        prod-ansible-com:
          description: "Tenant for the new ansible community website"
          display: "Tenant for ansible.com new website"
          groups:
            - ansible-admins

        stage-ansible-com:
          description: "Tenant for the new ansible community website (stage)"
          display: "Tenant for ansible.com new website"
          groups:
            - ansible-admins

        prod-webknjaz-s-project:
          description: "Tenant for the patchback project (prod)"
          display: "Tenant for the patchback project"
          groups:
            - patchback-admins

        dev-search-quarkus-io:
          description: "Tenant for testing search.quarkus.io"
          display: "Tenant for testing search.quarkus.io"
          groups:
            - quarkus-search-admins

        prod-search-quarkus-io:
          description: "Tenant for the search.quarkus.io project (prod)"
          display: "Tenant for the search.quarkus.io project"
          groups:
            - quarkus-search-admins

        prod-osci-announce:
          description: "OSCI Announcement System"
          display: "announce.osci.io"
          groups:
            - osci-admins

        prod-mvnpm-org:
          description: "Tenant for mvnpm.org"
          display: "Tenant for mvnpm.org"
          groups:
            - mvnpm-admins

        prod-cfp-osci-io:
          description: "Tenant for cfp.osci.io"
          display: "Tenant for cfp.osci.io"
          groups:
            - osci-admins

        prod-osci-operators:
          description: "Tenant to deploy cluster wide operators"
          display: "Tenant for osci stuff"
          groups:
            - osci-admins

        prod-langchain4j-github-bot:
          description: "Tenant for langchain4j github bot"
          display: "Tenant for langchain4j github bot"
          groups:
            - langchain4j-admins

        prod-hibernate-infra:
          description: "Tenant for a hibernate bot"
          display: "Tenant for hibernate jira bot"
          groups:
            - hibernate-admins

        prod-www-osci-io:
          description: "OSCI Website"
          display: "www.osci.io"
          groups:
            - osci-admins

        prod-cfp-fedoraproject-org:
          description: "Fedora CFP site"
          display: "cfp.fedoraproject.org"
          groups:
            - osci-admins

        prod-unconference-kubevirt-io:
          description: "Hedgedoc for Kubevirt"
          display: "unconference.kubevirt.io"
          groups:
            - osci-admins

        prod-rsvp-fedoraproject-org:
          description: "Fedora pretix site"
          display: "rsvp.fedoraproject.org"
          groups:
            - osci-admins

        prod-elections-llvm-org:
          description: "LLVM elekto instance"
          display: "elections.llvm.org"
          groups:
            - llvm-admins

        prod-camel-jbang-testing:
          description: "Camel jbang tenant for testing"
          display: "non web tenant for teting Camel jbang"
          groups:
            - camel-jbang-admins

        prod-analytics-ossupstream-org:
          description: "Tenant for Matomo instance"
          display: "analytics.ossupstream.org"
          groups:
            - ossupstream-admins
