---

- name: "Setup ML storage (LVM)"
  hosts: lists.nfs-ganesha.org
  vars:
    device: /dev/vdb
  tasks:
    - name: Add lvm on data disk
      community.general.filesystem:
        dev: "{{ device }}"
        fstype: lvm
    - name: Create a volume group on data disk
      community.general.lvg:
        pvs: "{{ device }}"
        vg: "{{ data_vg_name }}"

#use a separate task to avoid variable contamination
- name: "Deploy website (GH redirect)"
  hosts: lists.nfs-ganesha.org
  tasks:
    - name: Create vhost for www.nfs-ganesha.org
      ansible.builtin.include_role:
        name: httpd
        tasks_from: vhost
      vars:
        use_tls: True
        use_letsencrypt: True
        force_tls: True
        website_domain: www.nfs-ganesha.org
        redirect: https://github.com/nfs-ganesha/nfs-ganesha/wiki

- name: "Setup ML storage (partitions) and ML"
  hosts: lists.nfs-ganesha.org
  vars:
    vg: "{{ data_vg_name }}"
  roles:
    - role: lvm_partition
      size: 2G
      lv_name: mailman3
      path: /var/lib/mailman3
    - role: lvm_partition
      size: 1G
      lv_name: postgres
      path: /var/lib/pgsql
    - role: lvm_partition
      size: 512M
      lv_name: hyperkitty
      path: /var/lib/hyperkitty
    - role: swap_file
      size: 1G
      path: /var/swap
    - role: mailing_lists_server
      display_name: "NFS Ganesha List Archives"
      domain: lists.nfs-ganesha.org
      admin_users:
        - duck
        - misc
      mail_aliases:
        root: "{{ ['root'] + comminfra_tech_emails }}"
        listmaster: root
      use_simple_tls: True
      whitelist_clients:
        - redhat.com
  tags: mailinglists

- name: "Deploy stats upload/download vhost"
  hosts: lists.nfs-ganesha.org
  vars:
    website_domain: download.nfs-ganesha.org
    document_root: "/var/www/{{ website_domain }}"
    users:
      kkeithle:
        ssh_keys: "{{ tenant_users['kkeithle'].ssh_keys }}"
        shell: True
      staticanalysis:
        ssh_keys:
          - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjEU6xEkKusWawea3SzMNrzUdGc4plaom6yYto5YDjNZqFQBNECEILheiNrZuQr2jQmyYo4XLPQA90ODQOwQRPhsze8k+s4AMVZiBWrt0T1iIO0d8VdHP5eGGI57MtO3WPLknqPX3J+fM9Nb9S5bZ/R+Gjh1hVPl8V6La8S5MSv4AE/TSFFtdyzGUqexie2JN5/Bw6lz9w83tDOSw2utQvpELA5CYBxSt712776cYE9dVkOQrbM23+2r1n7DPQ9OF48GZB1d/TW3BrmKqIoSst4gFfm5TEWlee4IMquwWOjdfFTzI/aykbuGwnQ6myPutQHVIJrUu+5TDKzrzSjBjf staticanalysis@rhs-vm-11.gdev.lab.eng.bos.redhat.com"
    uploaders_group: upload
  tasks:
    - name: Create partition for download data
      ansible.builtin.import_role:
        name: lvm_partition
      vars:
        vg: "{{ data_vg_name }}"
        size: 2G
        lv_name: download
        path: "{{ document_root }}"
    - name: "Create Uploaders' group"
      ansible.builtin.group:
        name: "{{ uploaders_group }}"
        state: present
    - name: "Create uploaders' users"
      ansible.builtin.user:
        name: "{{ item.key }}"
        comment: "Uploader User"
        groups:
          - "{{ uploaders_group }}"
      loop: "{{ q('dict', users) }}"
      register: user_creation
    - name: Install uploaders' keys
      ansible.posix.authorized_key:
        key: "{{ item.1 }}"
        key_options: "{{ item.0.value.shell|default(False) | ternary(omit, 'command=\"internal-sftp\",no-port-forwarding,no-agent-forwarding,no-X11-forwarding,no-pty') }}"
        user: "{{ item.0.key }}"
        manage_dir: "{{ item.0.value.shell|default(False) }}"
      loop: "{{ q('subelements', q('dict', users), 'value.ssh_keys') }}"
      failed_when: "{{ not (ansible_check_mode or user_creation.changed) }}"
    - name: Harden security for uploaders configuration
      ansible.builtin.file:
        path: "~{{ item.0.key }}/.ssh/authorized_keys"
        owner: "{{ item.0.value.shell|default(False) | ternary(omit, 'root') }}"
        mode: "{{ item.0.value.shell|default(False) | ternary(omit, '0644') }}"
      loop: "{{ q('subelements', q('dict', users), 'value.ssh_keys') }}"
    - name: Harden security for uploaders configuration, .ssh directory
      ansible.builtin.file:
        path: "~{{ item.0.key }}/.ssh/"
        owner: "{{ item.0.value.shell|default(False) | ternary(omit, 'root') }}"
        mode: "{{ item.0.value.shell|default(False) | ternary(omit, '0750') }}"
        attr: "{{ item.0.value.shell|default(False) | ternary(omit, 'i') }}"
      loop: "{{ q('subelements', q('dict', users), 'value.ssh_keys') }}"
    - name: "Create vhost for {{ website_domain }}"
      ansible.builtin.import_role:
        name: httpd
        tasks_from: vhost
      vars:
        document_root_group: "{{ uploaders_group }}"
        use_tls: True
        use_letsencrypt: True
        force_tls: True
    - name: "Allow web index for {{ website_domain }}"
      # noqa no-tabs
      ansible.builtin.copy:
        content: "<Location />\n\tOptions +Indexes\n\tReadmeName README.txt\n</Location>"
        dest: "{{ _vhost_confdir }}/free_listing.conf"
        owner: root
        group: root
        mode: "0644"
      notify: reload httpd
      # TODO: investigate why noqa does not work here
      tags: skip_ansible_lint
  tags: download

