---

- name: "Setup ML storage (LVM)"
  hosts: lists.jboss.org
  vars:
    device: /dev/sdb
  tasks:
    - name: "Add LVM on data disk"
      community.general.filesystem:
        dev: "{{ device }}"
        fstype: lvm

    - name: "Create a volume group on data disk"
      community.general.lvg:
        pvs: "{{ device }}"
        vg: "{{ data_vg_name }}"

  tags: partitioning


- name: "Setup ML NFS for old archives"
  hosts: lists.jboss.org
  vars:
    uploader_login: jbossoldml
    uploader_key: "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzZ+9xDUNfXEoxKoXUlYWTYNps+BqkLCrBppAqLPitNPefGxPqEA0USj6hknBETRAOPZoux8/c4Gk14jn9n7QgcUfOKY/GxeoFqUWcq94bBAlfrlhdmdjQZGHaV++imYdCUFLzkR0VKFT4W6NtZkneCLgeBfzNBINmG/7eYbgayZ2XSlrMgOCtu1Cr53xHWtY7vaX5zlp+IfvoANjaH8Hw4pAifoyhk0RSo+iIn06HOf1U1Kyio/sFx5WwpgINEKDUgh2jvvy/K9c3b82MPasIqxzhMSIeHMg5JljS+BkMbUe4EVBhHu/aH6u+A13rOIBX/hhki3j/z27TtYEYGdUrQ== root@lists01.dmz-a.mwc.hst.phx2.redhat.com"
    uploader_home: /srv/data/mailman2
  tasks:
    - name: Install NFS tools
      ansible.builtin.package:
        name: nfs-utils
        state: present

    - name: "Mount NFS storage"
      ansible.posix.mount:
        src: "lucille.srv.osci.io:/{{ inventory_hostname }}"
        path: /srv/data
        fstype: nfs4
        opts: "defaults,nodev,nosuid,noatime,nodiratime,acl,x-systemd.automount"
        state: mounted

    - name: "Create uploader's user for ML archives sync"
      ansible.builtin.user:
        name: "{{ uploader_login }}"
        comment: "Uploader User for JBOSS old ML sync"

    - name: "Create directory for Mailman 2 archives"
      ansible.builtin.file:
        path: "{{ uploader_home }}"
        state: directory
        owner: "{{ uploader_login }}"
        group: "{{ uploader_login }}"
        mode: "0755"

    - name: "Install rsync for the ML archives sync"
      ansible.builtin.package:
        name: rsync

    - name: "Install sync SSH key"
      ansible.posix.authorized_key:
        key: "{{ uploader_key }}"
        key_options: "command=\"rsync --server -vlogtrze.isf --omit-dir-times . {{ uploader_home }}\",no-port-forwarding,no-agent-forwarding,no-X11-forwarding,no-pty"
        user: "{{ uploader_login }}"

    - name: Harden security for uploaders configuration
      ansible.builtin.file:
        path: "~{{ uploader_login }}/.ssh/authorized_keys"
        owner: root
        mode: "0644"

    - name: Harden security for uploaders configuration, .ssh directory
      ansible.builtin.file:
        path: "~{{ uploader_login }}/.ssh/"
        owner: root
        mode: "0750"
        attr: i


# some free space has been kept aside in the VG if needs be
- name: "Setup ML storage (partitions)"
  hosts: lists.jboss.org
  vars:
    vg: "{{ data_vg_name }}"
  roles:
    - role: lvm_partition
      size: 4.47G
      lv_name: mailman3
      path: /var/lib/mailman3

    - role: lvm_partition
      size: 15G
      lv_name: postgres
      path: /var/lib/pgsql

    - role: lvm_partition
      size: 52M
      lv_name: hyperkitty
      path: /var/lib/hyperkitty

    - role: lvm_partition
      size: 120G
      lv_name: mailman3_web
      path: /var/www/mailman

    - role: lvm_partition
      size: 4G
      lv_name: httpd_log
      path: /var/log/httpd

    - role: swap_file
      size: 1G
      path: /var/swap

  tags: partitioning


- name: "Deploy ML"
  hosts: lists.jboss.org
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/jboss/mail"
    # due to https://github.com/ansible/ansible/issues/21890
    sa_config_bits: /etc/mail/spamassassin/local.cf.d
  tasks:
    - name: Install Mailing-Lists Server
      ansible.builtin.include_role:
        name: mailing_lists_server
        public: yes
      vars:
        display_name: "JBoss List Archives"
        domain: lists.jboss.org
        admin_users:
          - "{{ 'duck' ~ '@redhat.com' }}"
          - "{{ 'mscherer' ~ '@redhat.com' }}"
        mail_aliases:
          root: "{{ ['root'] + comminfra_tech_emails }}"
          listmaster: root
        local_accounts:
          - issues
        use_simple_tls: True
        with_dovecot: True
        whitelist_clients:
          - redhat.com
        use_custom_favicon: True
        # DB is too huge to be saved locally
        db_backup_databases: []

    - name: "Configure web access to old ML archives"
      ansible.builtin.copy:
        src: "{{ data_dir }}/old_ml_archives.conf"
        # Duck: we should think about making _vhost_confdir part of the public API
        dest: "{{ _vhost_confdir }}/"
        owner: root
        group: root
        mode: "0644"
      notify: reload httpd

    - name: "Allow Apache to read files on NFS"
      ansible.posix.seboolean:
        name: httpd_use_nfs
        state: yes
        persistent: yes

    # needed for the 'synchronize' module
    - name: "Install rsync"
      ansible.builtin.package:
        name: rsync
        state: present

    - name: "Install Mail Templates"
      ansible.posix.synchronize:
        src: "{{ data_dir }}/mail_templates/{{ item }}/"
        dest: "/var/lib/mailman3/templates/{{ item }}/"
        owner: no
        group: no
        delete: yes
      loop:
        - site
      notify: restart mailman3

    - name: Install custom favicon
      ansible.builtin.copy:
        src: "{{ data_dir }}/{{ item }}"
        dest: "{{ webapp_path }}/static-extra/"
        owner: root
        group: root
        mode: "0644"
      loop:
        - favicon.ico
        - jboss_logo.png
        - username_norenaming.css
      notify: Update static files

    - name: Install custom branded navbar template
      ansible.builtin.copy:
        src: "{{ data_dir }}/{{ item }}"
        dest: "{{ webapp_path }}/templates/hyperkitty/"
        owner: root
        group: root
        mode: "0644"
      loop:
        - navbar-brand.html
        - headers.html
      notify: reload apache

    # quick fix for SPAM generating a low Spamassassin score
    - name: SPAM filtering
      ansible.builtin.copy:
        src: "{{ data_dir }}/sa_ovirt_mls.cf"
        dest: "{{ sa_config_bits }}/10_ovirt_mls.cf"
        owner: root
        group: root
        mode: "0644"
      notify: regenerate spamassassin configuration

    - name: Discard very-likely SPAM
      ansible.builtin.blockinfile:
        path: /etc/postfix/header_checks
        block: |
          /^X-Spam-Level: \*{8,}/ DISCARD
        marker: "# {mark} ANSIBLE MANAGED BLOCK ML-SPAM"
      notify: reload mail system

    - name: "Install Dovecot users"
      ansible.builtin.copy:
        src: "{{ inventory_dir }}/data/tenants/jboss/mail/users/"
        dest: /etc/dovecot/users/
        owner: root
        group: dovecot-proxy
        mode: "0640"

  tags: mailinglists

