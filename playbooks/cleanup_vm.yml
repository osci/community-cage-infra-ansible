---

# This playbook does:
# - destroy the source VM (undefine)
# - destroy the VM provisioning data on the source
# - destroy the VM LVs
#
# Additionally this playbooks checks for and sets the `moved` flag to avoid mistakes.
#

# You can skip the prompt by passing vars using the `-e` option

- name: "Remove obsolete VM"
  hosts: localhost
  vars_prompt:
    - name: hyp
      prompt: "Please provide the hypervisor"
      private: no

    - name: vm_name
      prompt: "Please provide the VM name (domain)"
      private: no

  tasks:
    - name: "Fetch hypervisor facts"
      ansible.builtin.setup:
      delegate_to: "{{ hyp }}"
      delegate_facts: True

    - name: "Fetch list of VMs in source hypervisor"
      community.libvirt.virt:
        command: list_vms
      register: vms_list
      delegate_to: "{{ hyp }}"

    - name: "Check that the VM already exists"
      ansible.builtin.fail:
        msg: "VM does not exist on the hypervisor"
      when: vm_name not in vms_list.list_vms

    - name: "Fetch list of VM status"
      community.libvirt.virt:
        command: status
        name: "{{ vm_name }}"
      register: vm_status_res
      delegate_to: "{{ hyp }}"

    - name: "Check the VM is stopped"
      ansible.builtin.fail:
        msg: "The VM is currently in use; stop it if you wish to proceed"
      when: vm_status_res.status != 'shutdown'

    - name: "Check if VM was moved"
      ansible.builtin.stat:
        path: "{{ hostvars[hyp].virt_moved_flag }}"
      register: vm_moved_flag_res
      delegate_to: "{{ hyp }}"

    - name: "Warn when VM was not moved"
      ansible.builtin.pause:
        prompt: "VM was not moved from this hypervisor; if you are sure press ENTER, else press Ctrl-C"
      when: not vm_moved_flag_res.stat.exists

    - name: "Find the VM provisioning config"
      ansible.builtin.find:
        paths:
          - "/etc/libvirt/kickstarts/{{ vm_name }}"
          - "/etc/libvirt/preseeds/{{ vm_name }}"
        file_type: file
      register: vm_prov_files_res
      delegate_to: "{{ hyp }}"

    - name: "Remove the VM provisioning config"
      ansible.builtin.file:
        path: "{{ item.path | dirname }}"
        state: absent
      loop: "{{ vm_prov_files_res.files }}"
      delegate_to: "{{ hyp }}"

    - name: "Get VM Config"
      community.libvirt.virt:
        command: get_xml
        name: "{{ vm_name }}"
      register: vm_config_res
      delegate_to: "{{ hyp }}"

    - name: "Get list of LVs to transfer"
      community.general.xml:
        xmlstring: "{{ vm_config_res.get_xml }}"
        xpath: /domain/devices/disk/source
        content: attribute
      register: vm_config_disks_path
      delegate_to: "{{ hyp }}"

    - name: "Remove LVs"
      vars:
        lv: "{{ disk.source.dev | basename }}"
      community.general.lvol:
        lv: "{{ vm_name }}"
        vg: "{{ hostvars[hyp].volgroup }}"
        state: absent
        force: True
      loop: "{{ vm_config_disks_path.matches }}"
      loop_control:
        label: "{{ lv }}"
        loop_var: disk
      delegate_to: "{{ hyp }}"

    - name: "Undefine VM"
      community.libvirt.virt:
        command: undefine
        name: "{{ vm_name }}"
      delegate_to: "{{ hyp }}"

    - name: "Unmark VM as moved on the destination"
      ansible.builtin.file:
        path: "{{ hostvars[hyp].virt_moved_flag }}"
        state: absent
      delegate_to: "{{ hyp }}"

