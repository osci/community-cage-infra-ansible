#!/bin/bash
cd /etc/pki/tls/private/
cp -f {{ website_domain }}.key pgbouncer.key 
chown pgbouncer pgbouncer.key

cd /etc/pki/tls/certs/
cp -f {{ website_domain }}.crt pgbouncer.crt
chown pgbouncer pgbouncer.crt
