# Ansible role to install a Gnocchi builder

[Gnocchi](http://gnocchi.osci.io/) is a multi-tenant timeseries, metrics and resources database.

This role aim as at building the Gnocchi's documentation and publish it.

Publishing is done using rsync, so it can be in a local directory or a remote server. In the later case you need to add a SSH key to the builder's user and authorize it on the remote end; this is outside the goal of this role.

## Variables

Main variables:

- **document_root**: directory to sync the documentation to when the build is successful, using the same syntax for the rsync destination parameters

Advanced configuration variables:

- **git_url**: override the default project URL to make your own custom build
- **git_url_client**: override the default client project URL to make your own custom build
- **git_version**: override the default version checked out to be built (default to 'HEAD')
- **git_version_client**: override the default version checked out to be built (default to 'HEAD')
- **builder_user_name**: override the default user name
- **builder_user_home**: override the default user home directory
- **builder_root**: override the default directory used to checkout and build the project documentation
- **builder_root_client**: override the default directory used to checkout and build the project documentation

