# data manipulation in ansible is annoying, so I decided
# to write my own filter, since python would be more suitable
# than jinja
def generate_list_ssh_keys(groups, users, group_name, regions):
    res = []
    for user in groups.get(group_name, []).get('members', []):
        count = 0
        for k in users.get(user, []).get('ssh_keys', []):
            for r in regions:
                res.append({'region': r, 'key': k, 'name': '%s_%s' % (user, count)})
            count += 1
    return res

class FilterModule(object):
    ''' filter to generate a single list for AWS ec2_keys '''

    def filters(self):
        return {
            'generate_list_ssh_keys': generate_list_ssh_keys
        }
