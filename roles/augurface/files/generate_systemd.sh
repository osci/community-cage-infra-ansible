#!/bin/bash
DIR_CHECKOUT=$(mktemp -d augurface_XXXXX -p /tmp)
NAME=augurface

cleanup() {
	rm -Rf $DIR_CHECKOUT
}
trap cleanup EXIT

cd $DIR_CHECKOUT
git clone https://github.com/chaoss/augur.git
cd augur 
buildah build -f util/docker/$NAME/Dockerfile -t $NAME:latest .

podman create -p 8080:8080 --name $NAME $NAME
podman generate systemd -n $NAME > /etc/systemd/system/$NAME.service
systemctl daemon-reload 
systemctl restart $NAME
podman image prune -f -a
