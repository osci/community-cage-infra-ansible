---

- name: "Check if VM was moved"
  stat:
    path: "{{ virt_moved_flag }}"
  register: vm_moved_flag_res

- name: "Warn when VM was moved"
  fail:
    msg: "VM previously moved from this hypervisor; if you are sure then please cleanup first"
  when: vm_moved_flag_res.stat.exists

- name: "Get facts from DNS servers"
  setup:
  delegate_to: "{{ item }}"
  delegate_facts: True
  when: host_vars[item].ansible_default_ipv4.address is not defined
  loop: "{{ groups['dns_servers'] }}"

- name: cleanup
  file:
    path: /etc/systemd/system/ospo_virt.service
    state: absent
  notify: Reload the service file -- guest_virt_install

- name: "Ensure shared VG symlink is recreated at boot time"
  template:
    src: virt-shared-path.tmpfiles
    dest: /etc/tmpfiles.d/virt-shared-path.conf
    owner: root
    group: root
    mode: "0644"
  notify: regenerate tmpfiles

- name: "Flush Handlers to ensure the shared VG symlink is ready"
  meta: flush_handlers

- name: "Create VM '{{ vm_name }}' using OSAS Settings"
  include_role:
    name: guest_virt_install
  vars:
    bridge: "{{ virt.bridges[main_iface_vlan] | mandatory }}"
    bridge_iface: "{{ ifaces_vlans_mapping[main_iface_vlan] | default('eth0') }}"
    network:
      ip: "{{ hostvars[vm_name].ansible_host }}/{{ cage_vlans[main_iface_vlan].subnet | ansible.utils.ipaddr('prefix') }}"
      gateway: "{{ cage_vlans[main_iface_vlan].gateway }}"
    # using ansible_host instead of ansible_default_ipv4.address because AWS hosts have RFC1918 IPs
    # and we need to use the external IP
    nameservers: "{{ groups['dns_servers'] | map('extract', hostvars) | map(attribute='ansible_host') | list }}"

- name: "Add extra interfaces for '{{ vm_name }}'"
  include_role:
    name: guest_virt_install
    tasks_from: bridge
  vars:
    bridge: "{{ virt.bridges[item] | mandatory }}"
  loop: "{{ extra_ifaces_vlans }}"

# new system with iface mapping
- name: "Manage interfaces for '{{ vm_name }}'"
  include_role:
    name: guest_virt_install
    tasks_from: bridge
  vars:
    bridge: "{{ virt.bridges[item.key] }}"
    bridge_iface: "{{ item.value }}"
  loop: "{{ q('dict', ifaces_vlans_mapping) }}"

