---

- name: Load settings based on distribution
  include_vars: "{{ item }}"
  with_first_found:
    - "sys_{{ ansible_distribution }}.yml"
    - "sys_{{ ansible_os_family }}.yml"

- name: Disable fingerprint auth
  command: "authconfig --disablefingerprint --update"
  changed_when: True
  when: (ansible_distribution == 'Fedora' and ansible_distribution_major_version|int < 28) or (ansible_os_family == 'RedHat' and ansible_distribution_major_version|int < 7)

- name: "Setup log rotation"
  template:
    src: logrotate.conf
    dest: /etc/
    owner: root
    group: root
    mode: "0644"

- name: "Setup log rotation"
  template:
    src: "logrotate.d/{{ item }}"
    dest: /etc/logrotate.d/
    owner: root
    group: root
    mode: "0644"
  loop:
    # added into logrotate.conf before EL8 and then split
    - btmp

- name: Manage DNS Settings
  tags: dns
  block:
    - name: Tell NM to not care about DNS Settings
      # noqa risky-file-permissions
      community.general.ini_file:
        path: /etc/NetworkManager/NetworkManager.conf
        section: main
        option: dns
        value: none
      register: nm_config
      when: ansible_os_family == 'RedHat'

    - name: Restart NM
      service:
        name: NetworkManager
        state: restarted
      when:
        - "ansible_os_family == 'RedHat'"
        - nm_config.changed

    - name: Configure DNS Resolution
      template:
        src: resolv.conf
        dest: /etc/resolv.conf
        owner: root
        group: root
        mode: "0644"

# before any repo action as it would fail
- name: "Purge obsolete pre-stream repo config"
  file:
    path: /etc/yum.repos.d/CentOS-PowerTools.repo
    state: absent
  when: ansible_distribution_major_version|int >= 8 and (ansible_distribution == 'CentOS' or ansible_distribution == 'RedHat')

- name: "Switch to CentOS Stream"
  command: "dnf -y swap centos-linux-repos centos-stream-repos"
  args:
    removes: /etc/yum.repos.d/CentOS-Linux-BaseOS.repo
  notify: DNF Distro Sync
  when: ansible_distribution == 'CentOS' and ansible_distribution_major_version|int == 8

# do not merge with next item, since epel need to be installed first
- name: "Install Epel (CentOS)"
  package:
    name: epel-release
    state: present
  when: ansible_distribution == 'CentOS' and ansible_distribution_major_version|int >= 7

- name: "Install Epel (RHEL)"
  package:
    name: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version|int }}.noarch.rpm"
    state: present
  when: ansible_distribution == 'RedHat' and ansible_distribution_major_version|int >= 7

# cannot use 'yum copr' as yum-plugin-copr is missing on CentOS despite Copr's doc
# also there is no Ansible module for it
- name: setup the OSAS Infra repo
  yum_repository:
    name: osas_infra
    description: Copr repo for OSAS ComInfra Team
    baseurl: "https://copr-be.cloud.fedoraproject.org/results/duck/osas-infra-team-rpm-repo/{{ rpm_repo_component }}/"
    gpgkey: "https://copr-be.cloud.fedoraproject.org/results/duck/osas-infra-team-rpm-repo/pubkey.gpg"
    repo_gpgcheck: False
    gpgcheck: True
    skip_if_unavailable: True
    state: present
    enabled: True
  when: ansible_os_family == 'RedHat'
  notify: Clean YUM Metadata

# allow for testing on selected machines
- name: setup the OSAS Infra repo
  yum_repository:
    name: osas_infra_devel
    description: Copr repo for OSAS ComInfra Team
    baseurl: "https://copr-be.cloud.fedoraproject.org/results/duck/osas-infra-team-rpm-repo-devel/{{ rpm_repo_component }}/"
    gpgkey: "https://copr-be.cloud.fedoraproject.org/results/duck/osas-infra-team-rpm-repo-devel/pubkey.gpg"
    repo_gpgcheck: False
    gpgcheck: True
    skip_if_unavailable: True
    state: present
    enabled: False
  when: ansible_os_family == 'RedHat'
  notify: Clean YUM Metadata

# extra repo needed for needrestart at least

- name: "Enable PowerTools RPM Repository (EL8)"
  # noqa risky-file-permissions
  community.general.ini_file:
    path: /etc/yum.repos.d/CentOS-Stream-PowerTools.repo
    section: powertools
    option: enabled
    value: '1'
    create: no
  when: ansible_distribution_major_version|int == 8 and ansible_distribution == 'CentOS'

- name: "Enable CRB RPM Repository (EL9+)"
  # noqa risky-file-permissions
  community.general.ini_file:
    path: /etc/yum.repos.d/centos.repo
    section: crb
    option: enabled
    value: '1'
    create: no
  when: ansible_distribution_major_version|int >= 9 and ansible_distribution == 'CentOS'

- name: "Packaging Setup for Debian systems"
  block:
    # nothing is going to flow in automatically as the repository is
    # tagged NotAutomatic+ButAutomaticUpgrades. You need to ass APT
    # preferences to selectively use DuckCorp's packages.
    - name: "Add DuckCorp repository"
      # noqa template-instead-of-copy
      copy:
        content: "deb https://repository.duckcorp.org/debian {{ ansible_distribution_release }} dc-net"
        dest: /etc/apt/sources.list.d/duckcorp.list
        owner: root
        group: root
        mode: "0644"

    - name: "Install gpg package for minimalist public key operations"
      apt:
        name: gpg

    - name: "Install DuckCorp Repository APT Key"
      apt_key:
        keyring: "/etc/apt/trusted.gpg.d/duckcorp.gpg"
        data: "{{ lookup('file', 'duckcorp_repository.gpg.key') }}"

  when: ansible_os_family == "Debian"

# after all repos are setup and before installing packages
- name: "Flush Handlers to Update Package Cache"
  meta: flush_handlers

- name: install base tools
  package:
    name: "{{ base_tools }}"
    state: present

- name: Ensure removing packages also remove their unused dependencies
  block:
    - name: Install YUM plugin to remove leaf packages
      package:
        name: yum-plugin-remove-with-leaves
        state: present
    - name: Ensure the YUM plugin do its job always but safely
      copy:
        # there is not database of voluntarily installed packages, so it can't be perfect
        # activate all the time but use 'exclude_bin' to avoid being too zealous
        content: "[main]\nenabled = 1\nexclude_bin = 1\nremove_always = 1"
        dest: /etc/yum/pluginconf.d/remove-with-leaves.conf
        owner: root
        group: root
        mode: "0644"
  when: ansible_pkg_mgr == "yum"

- name: Test if the host is an AWS instance
  stat:
    path: /etc/cloud/cloud.cfg.d
  register: cloud_init

- name: Preserve AWS instance hostname
  copy:
    dest: /etc/cloud/cloud.cfg.d/06_preserve_hostname.cfg
    src: 06_preserve_hostname.cfg
    owner: root
    group: root
    mode: "0644"
  when: cloud_init.stat.isdir is defined and cloud_init.stat.isdir

- name: "Install firewall"
  block:
    - name: "Install firewalld"
      package:
        name: firewalld
    # fixes "failed: iptables-restore v1.8.2 (nf_tables): Chain already exists" when calling firewall-cmd
    - name: "Install fixed iptables (Debian Buster)"
      package:
        name: iptables
        default_release: buster-backports
        state: latest
      when: ansible_os_family == 'Debian' and ansible_distribution_release == 'buster'
      notify: Restart firewalld
    - name: "Flush Handlers to Restart firewalld with fixed iptables"
      meta: flush_handlers
    - name: work around cloud image who disable firewalld
      systemd:
        name: firewalld
        masked: no
        enabled: yes   # one of 'state' or 'enabled' is compulsory
      when: ansible_distribution == 'Fedora'
    - name: start firewalld service
      service:
        name: firewalld
        state: started
        enabled: yes
    # not working by default when not using NM
    - name: "Assign iface to firewalld public zone (Debian)"
      ansible.posix.firewalld:
        zone: public
        interface: "{{ (ansible_virtualization_role == 'guest') | ternary('eth0', ('virt_hosts' in group_names) | ternary('virbr0', 'bond0')) }}"
        permanent: yes
        immediate: true
        state: enabled
      when: ansible_os_family == 'Debian'
      notify: Restart firewalld
    # empty on Debian (should be upstream default but maybe having not interface assigned caused this problem)
    - name: "Set firewalld default zone (Debian)"
      command: "firewall-cmd --set-default-zone=public"
      changed_when: True
      when: ansible_os_family == 'Debian'
  when: ansible_distribution == 'Fedora' or (ansible_os_family == 'RedHat' and ansible_distribution_major_version|int >= 7) or ansible_os_family == 'Debian'

- name: Install Kernel Module options for firewalling
  copy:
    src: firewall_kmod.conf
    dest: /etc/modprobe.d/
    owner: root
    group: root
    mode: "0644"
  notify:
    - Warn Reboot Needed

# regenerating GRUB will fail if this package is missing on a RAID system
# (grub2-probe won't find the RAID disk)
# it's small therefore install on all systems
- name: "Install mdadm"
  package:
    name: mdadm

# it can still be reenabled on-demand but we need to save memory
- name: "Disable crashkernel"
  lineinfile:
    path: /etc/default/grub
    regexp: '^(GRUB_CMDLINE_LINUX=".*)crashkernel=auto(.*)'
    line: '\1crashkernel=no\2'
    backrefs: yes
  notify:
    - Regenerate GRUB Config
    - Warn Reboot Needed

