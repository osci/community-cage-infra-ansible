# Ansible role for Network Configuration

## Introduction

This role setup network configuration on Red Hat systems.

IPv6 is supported if using the NM method.

Up to EL7 interfaces are unmanaged by NetworkManager to avoid conflicts, especially NM not behaving properly with binding interfaces.
On Debian and EL8+ NM keyfiles are used.

## Bugs

This role is affected by Ansible#49473, crashing if an IPv6 global address is already defined (because in this case the link-local address is missing from the facts). It is not fixed in 2.7.10, not sure about 2.8.0.

## Settings

TODO: explain VLAN and host settings
